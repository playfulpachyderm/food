# Marinating chicken
Basically, soak it in yogurt.  You can add stuff to the yogurt for bonus flavor.

## Ingredients
- yogurt
- salt
- spices (various)

## Directions
Mix yogurt, salt and spices.  Add chicken and mix it in, covering all of the chicken.
