# Utensils

==== CHECK DOLLAR STORE! ====

1 large pot
2 medium pots
1 small pot
Cast iron pan
Teflon-coated pan
Sieve
Can opener
Wooden spoon (ideally flat edged)
Stirry thing, possibly just more wooden spoons
Plastic cutting board
Sharp knives (consider ceramic)
Cheese grater
Whisk

Parchment
Ziploc
Aluminum foil
Plastic wrap
