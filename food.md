# Meals

- chick pea soup
- taco surprise
- omelette

# Recipes to try:

- chelsea buns
- quesadillas
- [falafels](http://allrecipes.com/recipe/25309/seans-falafel-and-cucumber-sauce/)
- [classic meatloaf](http://www.delish.com/cooking/recipes/a50262/meatloaf-recipe/)
- [also classic meatloaf](http://www.tasteofhome.com/recipes/traditional-meat-loaf)
- [Kefta, Tomato And Egg Tagine](http://www.foodrepublic.com/recipes/kefta-tomato-and-egg-tagine-recipe/)
- [Eggs in tomato sauce](https://www.youtube.com/watch?v=618QsMaVXp8)
- [Mexican rice skillet](http://www.delish.com/cooking/recipe-ideas/recipes/a47561/mexican-beef-n-rice-skillet-recipe/)


- [Firecracker chicken meatballs](http://littlespicejar.com/firecracker-chicken-meatballs/)
- [Thai curry chicken meatballs](http://chefsavvy.com/recipes/thai-red-curry-chicken-meatballs/)

- [mayo steak marinade](https://www.tastingtable.com/cook/national/easy-steak-marinade-recipe-mayonnaise-cooking-grilling-tip)
- [good marinade](http://allrecipes.com/recipe/143809/best-steak-marinade-in-existence/)
- [Gordon Ramsay's steak recipes](https://www.youtube.com/watch?v=JZVWkjFGCBI)

- [Martha Stewart's chicken stock recipe](https://www.youtube.com/watch?v=22oXSSETXBU)

- [Lemon garlic shrimp](http://www.delish.com/cooking/recipe-ideas/recipes/a55657/easy-lemon-garlic-shrimp-recipe/)
- [Smothered pork chops](http://www.delish.com/cooking/recipe-ideas/recipes/a55503/how-to-make-smothered-pork-chops-recipe/)
- [Salisbury steaks](http://www.delish.com/cooking/recipe-ideas/recipes/a54937/best-salisbury-steak-recipe/)
- [Chebureki](http://alyonascooking.com/2016/05/chebureki-dough-recipe-meat-turnovers/)
- [also chebureki](https://www.youtube.com/watch?v=x9NEXPoepO4)
- [Georgian cheese bread](http://simplyhomecooked.com/khachapuri-georgian-cheese-bread/)
- [kompot](https://www.youtube.com/watch?v=9bZk6wXCMCY)

Homemade burger buns (brioche): https://www.reddit.com/r/food/comments/7cf9h0/homemade_burger_buns_fresh_out_of_the_oven/dpqlesk
Spicy beef stew with carrot and tomatoes: https://www.reddit.com/r/food/comments/7cf9h0/homemade_burger_buns_fresh_out_of_the_oven/dpqlesk
Breakfast casserole: https://www.familyfreshmeals.com/2012/01/crockpot-breakfast-casserole.html
Beef ribs: https://www.reddit.com/r/Cooking/comments/7vfcdr/beef_ribs

Omelettes:
- https://www.lanascooking.com/hash-brown-omelet-skillets/ | Hash Brown Omelet Skillets
- https://www.delish.com/cooking/recipe-ideas/recipes/a15987/omelet-souffle-recipe-fw0711/ | Omelet Soufflé Recipe
- http://www.edibleperspective.com/home/2014/7/25/breakfast-friday-pesto-roasted-tomato-omelet.html#_a5y_p=2080087 | Breakfast Friday: Pesto + Roasted Tomato Omelet — Edible Perspective
- https://www.theseasonedmom.com/baked-western-omelet/ | Baked Western Omelet - The Seasoned Mom

shallot sage wine sauce

Casseroles:
http://www.foodnetwork.ca/recipe/mexican-layered-bean-casserole/19094/
http://www.countryliving.com/food-drinks/g1279/casserole-dinner-recipes/?slide=16
http://www.foodnetwork.ca/recipe/twice-baked-potato-casserole/15582/
https://life-in-the-lofthouse.com/teriyaki-chicken-casserole/
https://lilluna.com/cheesy-chicken-alfredo-casserole/
http://www.seriouseats.com/recipes/2016/10/chicken-cacciatore-red-peppers-recipe.html
make this immediatly: http://www.countryliving.com/food-drinks/recipes/a36144/meatball-spinach-baked-ziti-recipe/

Browse:
http://www.delish.com/cooking/menus/g1467/one-skillet-dinners/?slide=14
http://www.delish.com/cooking/recipe-ideas/recipes/a54115/garlicky-greek-chicken-recipe/
http://www.delish.com/cooking/recipe-ideas/recipes/a54432/cajun-parmesan-salmon-recipe/
http://www.seriouseats.com/2014/10/how-to-make-cassoulet-chicken-food-lab-french-casserole.html
https://www.youtube.com/watch?v=KXajiO-30Ow
https://www.maangchi.com/blog/how-to-cut-up-a-whole-chicken
http://content.jwplatform.com/previews/6HuZ1pDQ-3437L1PQ
http://content.jwplatform.com/previews/0rxsfcvU-3437L1PQ

Bunch of random stuff from the NY Times: https://cooking.nytimes.com/68861692-nyt-cooking/922671-cooking-for-the-storm-70-recipes

https://www.thespruceeats.com/what-is-an-ancho-chile-pepper-995560 | What Is an Ancho Chile Pepper?
https://www.thespruceeats.com/what-are-poblano-peppers-995741 | Poblano Peppers: Culinary Staple of Mexico and the Southwest
http://www.foodrepublic.com/recipes/make-german-pretzel-buns/ | German Pretzel Buns Are Easier To Make Than You Think!
https://www.leaf.tv/articles/types-of-egg-breads/ | Types of Egg Breads | LEAFtv
https://www.allrecipes.com/recipe/17486/brioche/ | Brioche Recipe - Allrecipes.com

# Sites

- Tasty youtube channel: https://www.youtube.com/channel/UCJFp8uSYCjXOMnkUyb3CQ3Q/videos?sort=p&view=0&flow=grid
- Martha Stewart channel: https://www.youtube.com/user/MarthaStewart/videos?flow=grid&view=0&sort=p
- Jamie Oliver channel: https://www.youtube.com/channel/UCpSgg_ECBj25s9moCDfSTsA
- Taste of Home: http://www.tasteofhome.com/recipes/course/dinner-recipes?page=4

https://www.reddit.com/r/recipes/
https://www.reddit.com/r/cookingforbeginners/top/
https://trycookin.com/
https://www.reddit.com/r/Cooking/
https://www.reddit.com/r/fitmeals/
https://www.reddit.com/r/AskReddit/comments/5mkqww/seriousreddit_what_is_your_best_recipe/
https://www.reddit.com/r/food/
https://www.reddit.com/r/GifRecipes/
https://www.reddit.com/r/EatCheapAndHealthy/top/
https://www.reddit.com/r/Cooking/comments/6lnqad/what_are_your_favorite_international_cookbooks/
https://www.cooksmarts.com/
