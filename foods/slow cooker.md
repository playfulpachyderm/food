# Slow cooker

- [Dad's beef stew](www.tasteofhome.com/recipes/classic-beef-stew)
	- [version 1](https://www.youtube.com/watch?v=qOIHZpKyzMs)
	- [version 2](https://www.youtube.com/watch?v=KGsnNhr5p9s)
- beef in beer
- [Slow cooker chicken teryaki](http://damndelicious.net/2015/12/16/slow-cooker-teriyaki-chicken-and-rice/)
- [Mongolian beef](http://therecipecritic.com/2015/08/slow-cooker-mongolian-beef/)
- [party meatballs](http://www.delish.com/cooking/recipe-ideas/recipes/a50599/slow-cooker-party-meatballs-recipe/)
- [Ribs](https://www.youtube.com/watch?v=1t60widjlhY)
- chicken parm soup
- [wine braised lamb semi-stew](https://www.tastingtable.com/cook/recipes/red-wine-braised-lamb-neck-recipe)
