# Burgers

- tips: http://aht.seriouseats.com/2010/03/the-burger-labs-top-ten-tips-for-better-burgers.html

- regular (beef)
- [Chicken guacamole burgers](http://www.delish.com/cooking/recipe-ideas/recipes/a43729/spicy-chicken-burgers-guacamole-cheddar-pickled-onions-recipe/)
- [Burgers with egg](http://www.delish.com/cooking/videos/a48688/how-to-make-egg-in-a-hole-burgers-video/?mag=del&list=nl_dnl_news&src=nl&date=081516)
- [Buffalo chicken bluecheese ranch burgers](http://www.delish.com/cooking/recipe-ideas/recipes/a42786/buffalo-chicken-burger-blue-cheese-ranch/)
- [chili cheese burgers](http://www.delish.com/cooking/recipe-ideas/recipes/a48951/chili-cheese-burgers-recipe/)
- [garlic cheeseburger sliders](http://www.delish.com/cooking/recipe-ideas/recipes/a47323/pull-apart-cheeseburger-slider-recipe/)
