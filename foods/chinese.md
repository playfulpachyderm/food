# Chinese food

- [fried rice](/recipes/fried_rice.md)
- [Honey sesame chicken (light)](https://www.youtube.com/watch?v=6iFMqk8uJqY)

- [more fried rice](http://www.delish.com/cooking/recipe-ideas/recipes/a47583/beef-fried-rice-recipe/)
- [overly battered chicken](https://www.youtube.com/watch?v=hfxledIyK6I)
- [General Tso chicken](http://www.seriouseats.com/recipes/2014/04/the-best-general-tsos-chicken-food-lab-chinese-recipe.html)
- More General Tso chicken (use cast iron to "shallow-fry" in 1 cm or so of oil): https://www.youtube.com/watch?v=_q5TuSR1uPs
- orange beef?
- [Orange Chicken](http://damndelicious.net/2013/10/19/chinese-orange-chicken/)
- [Orange ground chicken](http://www.frugalnutrition.com/orange-ground-chicken-rice-bowls/)
- [Mongolian beef](http://therecipecritic.com/2015/08/slow-cooker-mongolian-beef/)
- [spicy garlic fried chicken](https://www.youtube.com/watch?v=VjneaJ0hmgs)
- [spicy chicken and cheese](https://www.youtube.com/watch?v=T9uI1-6Ac6A)

Youtube channel:
https://www.youtube.com/user/Maangchi/videos?flow=grid&sort=p&view=0
