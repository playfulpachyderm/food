# Lemon shrimp pasta

Time: ?
Serves: 2-3 people

## Equipment

- 1 large pot
- 1 pot for cooking pasta (can be the same one)
- cutting board
- knife
- lemon juicer
- lemon zester
- 1 side bowl

## Ingredients

- 350g fusilli
- 400g shrimp, raw and peeled
- 5 cloves garlic
- 2 tbsp olive oil
- 1 bundle green onion (4-5 stalks)
- 1/2 cup white wine
- 1-2 tbsp butter
- 1 lemon

- salt
- pepper


## Directions

- cook fusilli

- finely chop / mince garlic and green onion

- heat pot to high temperature
- sear shrimp in little olive oil, salt and pepper, just until they turn pink on the outside
- take shrimp out and set aside
- turn down heat, add olive oil, cook garlic and green onion (1 min)
- deglaze with white wine, scrape up bits
- reduce
- juice and zest the lemon, add both to pot
- melt in butter
- toss with fusilli and shrimp

## Tips

- don't overcook the shrimp.  They become rubbery and not good

# Nutrition

Food                                Calories    Carbs       Protein     Fat         Sugar

350g pasta                          1236        259         46          7           10
400g shrimp                         248         0           60          4           0
30g olive oil                       265         0           0           30          0
28g butter                          200         0           0           23          0
100g green onion                    32          7           2           0           2

Total                               1981        266         108         64          12
                                                (54%)       (22%)       (29%)       (5%)
