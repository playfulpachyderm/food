# Chicken spinach cannelloni (or manicotti)
Original: http://bevcooks.com/2013/01/chicken-spinach-and-mushroom-manicotti/

## Equipment

- 1 large pot (for pasta sauce)
- 1 medium or large pot
- cutting board
- knife
- 1 or 2 casserole dishes (enough to hold all the cannelloni)


## Ingredients

- 225g manicotti (or 8 oz)

Sauce:
- [pasta sauce or ingredients for it](/recipes/pasta sauce.md)
- 1/3 cup cream

Filling:
- 450g ground chicken
- butter or olive oil
- 2 cloves garlic
- 140g spinach
- 2 tbsp finely chopped fresh parsley (optional)
- 300-400g ricotta cheese
- 1 cup shredded mozzarella
- 1 cup mozzarella extra for topping

Seasonings:
- chili flakes
- cinnamon (about 1-2 tsp)
- worcestershire sauce (about 2-3 tsp)
- pepper
- salt

Garnish (optional):
- grated parmegiano
- more chopped fresh parsley


## Directions

Cannelloni
- submerge cannelloni in water in the casserole dish to soak while cooking

Sauce:
- in large pot, cook pasta sauce (can be done concurrently, may be rate limiting factor)
- mix in cream at the end to make a rose sauce

Filling:
- in separate pot, cook ground chicken in small amount of oil until fully cooked / brown
- mince garlic and add to pot, cook until raw flavor is gone
- add seasonings
- shred spinach and add to pot, cook on low heat until spinach wilts (3-5 min)
- remove from heat and mix in ricotta, mozzarella, and fresh parsley
- pour small layer of sauce into casserole dish, just enough to cover the bottom
- stuff cannelloni with filling and arrange in casserole dish
- pour rest of sauce on top and cover with mozzarella layer
- bake at 350 for 30 min

- optional: garnish with freshly grated parmegiano and parsley


## Tips

- soak the cannelloni noodles for the WHOLE time.  They don't get soggy; if anything, they're still too firm.  Possibly even consider briefly boiling cannelloni noodles
- use the full amount of spinach.  It looks like a lot but the volume decreases to almost nothing when they wilt.


## Nutrition

Food                             Calories    Carbs       Protein     Fat         Sugar

250g manicotti                      877         180         31          2           9
500g chicken                        650         0           95          30          0
4 tbsp butter                       408         0           0           48          0
300g ricotta                        490         16          33          33          5
2 cups mozzarella                   560         8           72          32          0
2 onions                            80          18          2           0           8
28 oz tomatoes                      165         38          13          0           25
1/3 cup cream                       267         5           2           27          0

Total                               3497        265         248         172         47
                                                (30%)       (28%)       (44%)       (17%)
