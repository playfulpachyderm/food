# Chicken Parmigiana Soup

Time: 1h
Serves: 4 - 5 people

Equipment:

- 1 large pot
- cutting board
- knife

Seasonings:

- large pinch of salt
- cayenne powder
- garlic powder
- onion powder (optional)
- italian seasoning

Ingredients:

- 2 onions, chopped
- butter
- olive oil

- 3 cloves garlic, minced
- 3 tbsp tomato paste

- 28 oz crushed tomatoes
- 1-1.5L chicken broth

- 450g chicken breast, chopped into small cubes (about 2 breasts)
- 300g penne
- 300g mixed mozzarella and parmesan cheeses


Instructions:

- In large pot, heat butter and oil.  Cook onions to 1/2 cooked (translucent and soft, starting to turn golden-brown)
- Add garlic, cook 1-2 minutes

- stir in tomato paste
- add crushed tomatoes and chicken broth, bring to a simmer
- salt and season the soup, tasting until seasonings are right
- add cubed chicken and boil until chicken is fully cooked
- add pasta and boil until it is cooked "al dente"
- add half the cheese and stir it into the soup
- sprinkle remaining cheese on top and serve

Tips:

- salt more if chicken broth is "low sodium"


# Nutrition

Food								Calories	Carbs		Protein		Fat			Sugar

2 onions							88			20			3			0			10
1/4 cup butter						400			0			0			48			0
2 tbsp olive oil					226			0			0			28			0
3 tbsp tomato paste					39			9			2			0			6
28 oz crushed tomatoes				254			58			13			2			29
1L chicken broth					66			6			6			0			6
450g chicken breast					742			0			140			16			0
300g penne							1067		224			37			5			11
300g mozz / parm cheese				840			9			84			51			3

Total								3722		326			285			150			65
												(35%)		(31%)		(36%)		(20%)
