# Penne a la vodka

Ingredients

- 300g pancetta, thick cut
- 450g penne
- 1 cup 35% cream
- 300 mL vodka
- 1 package fresh chili peppers (I think they're cayenne)
- 1 large or 2 medium garlic cloves, not chopped
- salt
- 3 spooons tomato paste
- grated parmegiano (optional)
- fresh basil
- olive oil

Directions

Cut pancetta into cubes / strips.  Cut cayennes into small squares, removing all seeds.  Crush garlic cloves with side of knife, removing skin.
On medium heat, add very little oil and cook pancetta, cayenne squares, and crushed garlic (pancetta will release lots of oil).
Cook until pancetta becomes crispy-ish.  (Start cooking pasta now.)
Add vodka, let most of it boil out.
Pour in cream and add tomato paste (enough to make it turn pink).  Toss in fresh basil.
Mix in cooked pasta.  Serve with grated parmegiano.

Tips

- Don't cut cayennes too small.  Squares ~1cm on a side are a good size.
- Try: roll the cayennes firmly between palms to loosen the seeds, then cut the top and shake them out



# Nutrition

Food								Calories	Carbs		Protein		Fat			Sugar

300g pancetta						1072		10			42			96			0
450g penne							1600		336			56			8			16
1 cup cream							414			3			2			44			0
300 mL vodka						231			57			0			0			0
3 tbsp tomato paste					39			9			2			0			6
1 tbsp olive oil					119			0			0			14			0

Total								3475		415			102			162			22
												(48%)		(12%)		(42%)		(5%)
