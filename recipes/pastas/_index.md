# Pastas

- [basic pasta sauce](/recipes/pasta sauce/standard.md)
- [Buffalo chicken meatball pasta](/buffalo chicken meatball pasta.md) <-- don't skimp on the sauce!  It doesn't get very spicy.  Use danish blue.  Consider adding cream at the end?
- [Chicken cannelloni](/chicken spinach cannelloni.md)
- [penne alla vodka w/ pancetta](https://www.simplyrecipes.com/recipes/shrimp_pasta_alla_vodka/)

- [Cheeseburger noodle skillet](http://www.delish.com/cooking/recipe-ideas/recipes/a50293/cheeseburger-noodle-skillet-recipe/)
- [Swedish meatball egg noodles](https://www.reddit.com/r/GifRecipes/comments/4lq36g/onepot_swedish_meatball_pasta/)
- [bolognese recipe](https://www.youtube.com/watch?v=-gF8d-fitkU)
- [chicken bolognese recipe](http://www.foodnetwork.com/recipes/chicken-bolognese-with-penne-recipe-1942435)
- mac and cheese
	- [version 1](https://www.youtube.com/watch?v=MFGlSyrWkBk)
	- [version 2](https://www.youtube.com/watch?v=UEUjx2cPyaY)
- [real italian gravy](https://www.youtube.com/watch?v=-h6ToaL6Qg8)
- [baked meatball ziti](http://www.countryliving.com/food-drinks/recipes/a36144/meatball-spinach-baked-ziti-recipe/)
- [lemon shrimp pasta](/lemon shrimp pasta.md)
- [shrimp pasta alla vodka](https://www.simplyrecipes.com/recipes/shrimp_pasta_alla_vodka/)

## Chicken parmesianas

- regular
- [Chicken parmesiana soup](http://www.delish.com/cooking/recipe-ideas/recipes/a44085/chicken-parm-soup-recipe/)

- [Chicken parmesiana meatballs](http://www.delish.com/cooking/recipe-ideas/recipes/a46092/chicken-parm-meatball-skillet-recipe/)
- [Chicken parmesiana lasagna](http://bakerbynature.com/lightened-up-chicken-parmesan-lasagna/)
