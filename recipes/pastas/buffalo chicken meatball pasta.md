# Buffalo chicken meatball pasta
(original: http://www.delish.com/cooking/recipes/a45867/buffalo-chicken-meatballs-with-penne-recipe/)

Time: 1h15m
Serves: 4 - 5 people

## Equipment

- 1 small pot
- 1 large pot
- cutting board
- knife
- baking sheet

## Ingredients

Buffalo sauce:

- 1/4 cup butter
- 3/4 cup Frank's Red Hot sauce
- 125g danish blue cheese

Meatballs:

- 2 onions
- 4-5 green onions, chopped
- 2 large garlic cloves, minced
- large pinch of cayenne powder
- garlic powder
- 1/2-1 cup Panko breadcrumbs
- 1-2 eggs
- 450g ground chicken

- salt
- olive oil (not too much!)

- 1/4 cup cream
- 450g pasta (1 package), more if necessary


## Directions

- chop and fry onions to about 1/3 cooked, just turning translucent

    Buffalo sauce:
    - in small pot, melt butter, crumble blue cheese, and add hot sauce
    - stir / whisk until cheese is fully incorporated (2 min).
    - stir in 1/3 of the onion
    - taste and add salt if necessary

    Meatballs:
    - in large bowl, combine all the meatball ingredients except the chicken, and half the buffalo sauce
    - add a pinch of salt and mix together
    - once seasonings are mixed, add in chicken
    - grease a baking sheet
    - form into meatballs about 4-5 cm in diameter
    - bake at 425 until browned and slightly crunchy surface

- cook pasta
- once pasta is cooked, mix with baked meatballs and the remaining buffalo sauce
- mix in cream to reach desired texture


## Tips

- make sure everything is sorted out before you start mixing in raw chicken, because washing hands every time is a pain
- garnish with some extra green onion and crumbled blue cheese
- don't use too much oil for the onions, or meatballs get greasy / soggy
- don't oversalt the pasta (sauce has a fair bit of salt already)
- don't make meatballs too big (for optimal consistency), crumble some of the meat before baking so you can use the whole package of pasta (450g)


# Nutrition

Food                                Calories    Carbs       Protein     Fat         Sugar

1/4 cup butter                      408         0           0           48          0
125g danish blue cheese             408         3           26          36          1
1 tbsp olive oil                    113         0           0           14          0
2 onions                            80          18          2           0           8
5 green onions                      32          7           2           0           2
1 cup Panko breadcrumbs             220         46          6           1           2
2 eggs                              154         2           14          10          0
450g ground chicken                 729         0           81          45          0
450g pasta                          1588        334         58          8           10
1/4 cup cream                       200         0           1           20          0

Total                               3932        410         190         182         23
                                                (42%)       (19%)       (42%)       (6%)
