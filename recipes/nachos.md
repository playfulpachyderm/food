# Nachos

## Ingredients

- nacho chips
- shredded cheese
- assorted veggies (onions, green onions, tomatoes, peppers, hot peppers (jalapeno, cayenne, etc))
- choice of meat (see below)

## Directions

1. lay out bed of nacho chips on baking sheet
2. sprinkle with cheese, veggies and meat
3. bake until cheese melts

## Meats

### Buffalo chicken

- chicken breasts (thin cut)
- flour
- egg or other breading moistener
- ketchup
- frank's buffalo wing sauce

1. cut chicken breasts into small-bite-sized pieces
2. coat pieces in egg and bread them in flour
3. fry the chicken (fully cooked)
4. mix ketchup and frank's sauce to make buffalo chicken sauce
5. coat chicken in sauce

### Simple ground beef

- ground beef
- taco seasoning mix

1. cook beef fully and drain excess fat
2. mix in taco seasoning
