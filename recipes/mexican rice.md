Mexican rice
     1 tbsp. extra-virgin olive oil
    1 large onion, chopped
    1 lb. ground beef
    1 c. white rice
    1 15-oz. can fire-roasted tomatoes
    1 15-oz. can black beans, rinsed and drained
    1 1/2 c. corn (fresh, frozen, or canned)
    1 4-oz. can green chilis
    1 tbsp. ground cumin
    1 tbsp. chili powder
    kosher salt
    2 c. low-sodium chicken broth
    1 1/2 c. shredded cheddar and Monterey jack
    Fresh cilantro, for garnish

Directions

    In a large skillet over medium heat, heat oil. Add onion and cook 2 minutes, then add ground beef and cook until no longer pink, 6 minutes, breaking up with a wooden spoon as it browns. Drain fat.
    Move beef to one side of skillet and add white rice to other side. Cook 2 minutes, then add fire-roasted tomatoes, black beans, corn, green chilis, cumin and chili powder. Season with salt and stir to combine, then pour over chicken broth.
    Bring to a simmer, cover, and cook 20 minutes.
    Top with cheddar and Monterey Jack and cover to let melt, 2 to 3 minutes.
    Garnish with cilantro and serve.


# Nutrition

## Food                                 Calories    Carbs       Protein     Fat         Sugar

30g olive oil                           265         0           0           30          0
150g rice                               548         120         10          2           0
500g lean ground beef                   1210        0           107         85          0
425g tomatoes                           85          21          1           0           13
540g black beans                        562         97          38          5           5
14g olive oil                           124         0           0           14          0
onion                                   40          9           1           0           4
350g corn                               284         66          10          4           14
150g texmex cheese                      549         10          34          45          0

Total                                   3667        323         201         185         36
                                                    (35%)       (22%)       (45%)       (11%)

