# Fried rice

## Ingredients

- 1 cup uncooked brown rice or 2 cups cooked

- 1 medium or large onion
- 3 eggs
- 1/4 to 1/3 cup of peas
- 1/4 to 1/3 cup of carrots
- 220g meat (equivalent to 1 chicken breast)
- 4 green onions
- 1 cup bean sprouts

- Olive oil
- Sesame oil
- Soy sauce

- 1 wok
- 1 medium pot
- 3 side bowls

## Directions

### Prepare the components

Rice:

1. Cook the rice (boil for 15 min) and drain it

Onion:

1. Chop onion
2. Add some olive oil to the wok and (stir) fry the onion until they begin to brown
3. Store onion in side bowl

Egg:

1. Beat eggs with a few drops of soy sauce and a few drops of sesame oil
2. Add oil to the wok and fry egg fully
3. Crumble / chop it into small pieces and store in side bowl

Green onion:

1. Chop and store in side bowl

Stir fry:

1. Chop meat into thin pieces
2. Heat some olive oil and a few drops of sesame oil in wok
3. stir fry peas and carrots until they are un-frozen
4. Add chicken and onion, stir fry until chicken is cooked (about 2-3 min)
5. Add rice, green onion, bean sprouts.  Stir fry 3 min.
6. Add egg, soy sauce. Stir fry until everything is mixed.
