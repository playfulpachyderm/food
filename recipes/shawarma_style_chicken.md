# Shawarma-style chicken

## Ingredients

- 2 lbs chicken (2 breasts or 4 thighs)
- 1 onion

- [shawarma seasoning](/mixes/shawarma)

## Directions

1. slice chicken into pieces
2. marinate in spice blend
3. oven for about 15 minutes, turning pieces halfway
4. chop onion
5. take chicken out of oven and saute in skillet with onions


- have not tried: [Tahini sauce](/mixes/tahini_sauce)
