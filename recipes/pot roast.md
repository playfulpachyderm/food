
- Grandma's pot roast
	- [version 1](https://www.youtube.com/watch?v=4US87WBAGkk)
	- [version 2](https://www.youtube.com/watch?v=c1_V2y_9KCM)
- bohemian version: https://www.tasteofhome.com/recipes/bohemian-pot-roast

- chicken pot roast: http://www.houseandgarden.co.uk/recipes/main-courses/pot-roast-chicken

# Nutrition

## Food									Calories	Carbs		Protein		Fat			Sugar

3 onion                                 120         27          3           0           12
870g potatoes                           670         148         17          0           9
200g carrots                            82          20          2           0           10
200g celery								0			0			0			0			0
40g flour                               133         29          5           0           0

Total                                   1005        224         27          0           31
                                                    (89%)       (11%)       (0%)        (14%)

1106g top sirloin roast                 2356        11          299         122         0

Total                                   3361        235         326         122         31
                                                    (28%)       (39%)       (33%)       (13%)
Post cook beef: 616g
post cook veggies: 1483g

