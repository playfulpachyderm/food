# Sandwiches

- catalogue: http://www.seriouseats.com/a-sandwich-a-day
- more: https://en.wikipedia.org/wiki/List_of_sandwiches

- [Beef steak crunch wrap](https://gfycat.com/MintyCornyBug)
- [caprese sandwich](http://www.seriouseats.com/2011/04/caprese-sandwich-from-pane-bianco-phoenix-az-arizona.html)

## Grilled Cheese

- [grilled cheese](http://www.delish.com/cooking/recipe-ideas/g2815/best-grilled-cheese-recipes/)
- [French toast grilled cheese](http://www.delish.com/cooking/recipe-ideas/recipes/a52626/french-toast-grilled-cheese-recipe/)
- [Caprese chicken grilled cheese](http://www.delish.com/cooking/recipe-ideas/recipes/a52227/caprese-chicken-grilled-cheese-sandwich-recipe/)
- [grilled cheese + tomato soup](http://www.delish.com/cooking/recipe-ideas/recipes/a44539/creamy-tomato-basil-soup-grilled-cheese-bites-recipe/)
- [cuban sandwiches](http://www.foodnetwork.com/recipes/tyler-florence/the-ultimate-cuban-sandwich-recipe-1945003)
	- [v2](www.seriouseats.com/recipes/2016/07/cuban-sandwich-recipe.html)
	- [v3](www.seriouseats.com/recipes/2016/06/cuban-roast-pork-shoulder-mojo-recipe.html)
- [Croque Monsieur](https://www.thekitchn.com/videos/OlrRh4cK)
