# Tourtiere

## Ingredients
- 2 deep dish pie shells
- 700g ground meat (beef, pork, veal)
- 2 onions
- 3 or 4 cloves of garlic (optional)
- butter
- olive oil
- maple syrup (optional)

Spices:

- ground cinnamon
- ground cloves
- ground nutmeg
- ground allspice
- salt
- pepper
- garlic powder

## Directions

### Filling
1. in large skillet, fry onions in butter, also garlic if using raw garlic
2. store onions on separate plate.  (don't cook with meat, otherwise onion sugars and flavor will be poured out when straining)
3. in skillet, add oil, throw in meat and cook fully
4. drain meat thoroughly using strainer and put back in skillet
5. add the onions and more oil to the meat
6. mix in spices (don't go overboard on cloves) and a touch of maple syrup for sweetness
7. taste and reseason as necessary

### Preparation
1. fill up a pie shell with meat mix
2. using finger dipped in water, wet all the way around the edge of both pie shells so they will stick together
3. put second pie shell on top to close off pie
4. bake pie at 350 F for like 20 - 30 min
5. let pie cool for a few minutes before serving
