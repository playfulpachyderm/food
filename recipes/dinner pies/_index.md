# Dinner pies

- [tourtiere](/recipes/tourtier.md)
- [shepherd's pie](http://www.delish.com/cooking/recipe-ideas/recipes/a44629/beef-spinach-shepherds-pie-recipe/)

Pot pies:

- [version 1](http://www.tasteofhome.com/recipes/favorite-chicken-potpie)
- [version 2](http://www.familyfreshmeals.com/2016/03/best-homemade-chicken-pot-pie.html)
- [version 3](https://www.tastingtable.com/cook/recipes/chicken-potpie-recipe)
- [with turkey](http://www.tasteofhome.com/recipes/turkey-potpies)
