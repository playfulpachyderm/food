# Chicken pot pie

Equipment:

- 1 large pot
- 1 small casserole dish with lid (oven safe)
- cutting board
-

Ingredients:
- 2 deep dish pie shells
- 2 chicken breasts


- 1 large potato
- 1 carrot
- 4 tbsp flour
- 1 L chicken stock
-




- 4 pie shells (2 boxes)

- 3 chicken breasts
- 1 large potato
- 2 onions
- 3 garlic cloves
- 1 chopped carrot
- 2/3 cup frozen peas
- 6 tbsp flour
- 1/2 cup cream
- 750 mL chicken stock
- 3/4 cup butter

- salt, pepper
- thyme
- parsley


Directions:
	Chicken:
	- melt 1/4 cup butter in microwave
	-





# Nutrition

Food								Calories	Carbs		Protein		Fat		Sugar

1 cup butter						1627		0			2			184		0
1.5 russet potatoes					424			96			11			0		4.5
2 onions							132			30			3			0		15
3 garlic cloves						15			3			0			0		0
1 chopped carrot					30			7			1			0		3.5
6 tbsp flour						170			35			5			0		0
750 mL chicken stock				50			5			5			0		5
3 chicken breasts					852			0			162			18		0
1.5 cups frozen peas				177			32			12			1		12
1/2 cup cream						207			2			1			22		0
4 pie shells						3520		320			32			224		32

Total								7204		530			234			449		72
												(29%)		(13%)		(56%)


## Tips

- add sriracha!
- don't go overboard with flour.  It will turn into a gelatinous mess no matter how much chicken stock you add.
