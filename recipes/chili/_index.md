# Chili

- [Standard red wine chili](/recipes/chili.md)
- [turkey chili](http://www.cookingclassy.com/turkey-chili/)
- [also turkey chili](http://www.delish.com/cooking/recipe-ideas/recipes/a44384/spicy-turkey-chili-recipe/)
- [white chili](http://www.gimmesomeoven.com/5-ingredient-easy-white-chicken-chili-recipe/)
- [queso chicken and roasted corn chili](http://pinchofyum.com/queso-crockpot-chicken-chili-with-roasted-corn-and-jalapeno)
- [barbeque bacon cheeseburger chili](http://www.ihearteating.com/barbecue-bacon-cheeseburger-chili/)
- [shredded beef chili](http://www.recipetineats.com/shredded-beef-chili-con-carne/)
- [chili cheese garlic rolls](https://tasty.co/recipe/chili-cheese-garlic-bread-rolls)

Mac and cheese chilis:

- [mac and cheese chili](http://damndelicious.net/2014/03/15/one-pot-chili-mac-cheese/)
- [another mac and cheese chili](http://www.delish.com/cooking/recipe-ideas/recipes/a49097/chili-mac-cheese-recipe/)
- [more mac and cheese chili](www.delish.com/cooking/videos/a49096/chili-mac-cheese-video/)

Chili burgers:

- [chili cheese burgers](http://www.delish.com/cooking/recipe-ideas/recipes/a48951/chili-cheese-burgers-recipe/)
- [chili sliders](http://www.delish.com/entertaining/recipes/a49263/chili-cheese-sliders-recipe/)
