# Chili
Source: http://www.girlversusdough.com/2014/12/11/dads-beef-red-wine-chili/

Seasoning

- 2-3 tbsp chili powder
- 2 tsp smoked paprika
- 1 tsp cumin
- salt and pepper
- 2 tsp oregano

optional:

	2 tsp cocoa powder
	0.5 tsp coriander powder


Ingredients

- 2 tablespoons olive oil
- 2 yellow onions, chopped
- 1 green bell pepper, chopped
- 1 red bell pepper, chopped
- 2 teaspoons minced garlic
- 1 package bacon
- 1 lb (at least 80/20) ground beef
- 1 cup red wine (Merlot, Pinot Noir or Cabernet Sauvignon are some good options)
- 28 oz crushed tomatoes
- 1 chipotle chile in adobo sauce
- 30 oz black beans
- 15 oz dark red kidney beans
- 1 cup beef broth (optional)

Directions

In a large, heavy-bottomed saucepan or skillet over medium high heat, heat oil. Add onions and bell peppers. Saute 4 to 5 minutes until golden brown and softened. Add garlic; cook 1 minute.
Add ground beef; break apart with a wooden spoon and cook 10 minutes until browned.
optional: drain beef?
Pour in red wine. Increase heat to high; boil 10 minutes, stirring often and scraping up any bits that stick to bottom of pan. Reduce heat to a simmer.
In a food processor or blender, puree crushed tomatoes and chipotle chile pepper until smooth. Add to pan along with beans (do not drain). Stir in chili powder, smoked paprika, cumin and salt and pepper to taste. If chili is too thick, pour in some beef broth.
Cover and simmer 1 hour, stirring occasionally. Adjust seasonings as needed, and add more beef broth to thin out as needed. Serve with your favorite toppings.

Valid for slow cooking.


Toppings:

- sour cream
- cheese
- jalapenos
- green onion
- crushed tortillas
- sliced avocado
- egg
- serve on baked potato


# Nutrition

## Food								Calories	Carbs		Protein		Fat			Sugar

500g bacon							1290		8			59			107			8
2 onions							80			18			2			0			8
2 tbsp olive oil					238			0			0			28			0
520g beef 							1300		0			88			104			0
398mL kidney beans					350			64			22			2			5
1080mL black beans					1122		198			78			8			8
28 oz tomatoes						174			29			6			0			23

Total 								4554		317			255			249			52
												(28%)		(22%)		(49%)		(16%)

Mass: 3226
