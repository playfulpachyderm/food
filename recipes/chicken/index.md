# Chicken

- [shawarma-style chicken](/recipes/shawarma_style_chicken.md) <-- this never works.  doesn't taste right.
- [buffalo chicken nachos](/recipes/nachos.md#buffalo_chicken)
- [Sesame chicken ramen](http://www.delish.com/cooking/recipe-ideas/recipes/a47632/sesame-chicken-ramen-skillet-recipe/)
- [Southwest buttermilk baked chicken](http://damndelicious.net/2014/11/15/southwest-buttermilk-baked-chicken/)
- [garlic breaded chicken breasts](http://www.tasteofhome.com/recipes/garlic-lover-s-chicken)
- [turkish chicken, potatoes and bechamel](https://www.youtube.com/watch?v=WFqw5V-p5Z4)
- [bro diet chicken with flavoring](http://www.tasteofhome.com/recipes/rosemary-chicken-breasts)
- [chicken cacciatore](http://www.seriouseats.com/recipes/2016/10/chicken-cacciatore-red-peppers-recipe.html)
- [roasted garlic rosemary chicken](http://www.delish.com/cooking/recipe-ideas/recipes/a55097/garlic-rosemary-chicken-recipe/)
## with potatoes

- [communist chicken](/recipes/communist_chicken.md)

- shawarma poutine?  fries, gravy recipe, shawarma meat, hummus, garlic sauce, feta, garlic potatoes, tabouli, hot sauce, tahini (?)
- ramados style poutine
- Christmas dinner soup (turkey / chicken, stuffing, mashed potatoes, gravy, hot sauce)
- Bunch of stuff: https://www.youtube.com/watch?v=dfR_LdA3fPI
