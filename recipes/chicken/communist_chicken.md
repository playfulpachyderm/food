# Communist chicken
(So named, because it's red.)

## Ingredients
- 2 chicken breasts
- 1 large potato
- 2 medium onions
- 1 red pepper
- 4 to 6 eggs
- olive oil and/or butter
- salt

Spices:

- 2 tbsp paprika
- 1.5 tbsp chili powder
- 3 tsp garlic powder
- 1 to 2 tsp cayenne powder
- 1 to 2 tsp salt
- pepper (optional)



## Directions


### Spice mix

1. Mix them all together.  ez

Note that amounts are kind of arbitrary, but here's some guidelines:

1. It's mostly paprika and chili powder
2. You want slightly more paprika than chili powder (but not a lot more)
3. Consider how much you want to smell like garlic afterward
4. Spicier is better if you like that
5. Add extra salt separately while cooking


### Chicken

0. [Marinate](/marinade.md) the chicken for at least 3 hours
1. cut chicken into bite-sized pieces
2. heat oil in skillet (fairly hot but not too hot)
3. add small amount of spices to oil
4. add chicken
5. cover chicken in spices and let cook a bit
6. flip chicken and add more spices
7. continue cooking until acceptable sear is achieved (shouldn't take long)
8. add small amount of water to skillet
9. put skillet in oven for like 20 min
10. take chicken out and put it somewhere (keep the skillet and all liquid in it)

Optional step if a lot of stuff is fused to the pan:

11. add water to skillet and heat over the stove, use fork / spatula / other utensil to loosen aforementioned stuff

### Potatoes + veggies
1. chop potatoes and any optional veggies into bite-ish sized pieces
2. put in skillet with oil and drippings from chicken still in it
3. add bonus oil and spices and put over medium heat, stirring to cover all the veggies in oil and spices
4. oven them for like 20-30 min or until potatoes soften
5. add chicken back in and mix for maximum flavor combination

Dispense (serve) into a bowl, to free the skillet for frying the eggs.

### Eggs
1. melt butter in skillet over low heat
2. crack eggs into skillet
3. scramble and fry until fully cooked
4. crumble over top of potatoes and chicken


## Nutrition

Food								Calories	Carbs		Protein		Fat			Sugar

450g chicken breast					536			0			103			14			0
500g russet potato					395			90			11			0			3
500g russet potato					395			90			11			0			3
2 onions							80			18			2			0			8
6 eggs 								480			6			42			36			6
1/4 cup olive oil					476			0			0			56			0

Total								2362		204			169			106			17
												(23%)		(32%)		(49%)		(15%)
