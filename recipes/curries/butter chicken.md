# Butter Chicken

## Ingredients

- 2 chicken breasts
- butter
- naan or rice
- 1 medium onion

Spices:

- 1 tbsp ginger-garlic paste
- 1 tsp cumin powder
- 1 tbsp coriander powder
- 2 tbsp chili powder
- 1 tsp kasoori methi powder
- 2 tbsp crushed fried onion
- pinch of salt
- pinch of sugar

Gravy:

- 4 tbsp butter
- 1/2 tsp all-purpose flour
- 1 cup tomato puree OR 1 tbsp tomato paste
- 1/2 cup cream

## Directions

### Chicken
1. melt butter in skillet
2. Fry outside of the chicken
3. oven chicken for 15-20 min or fully cooked.  Don't cook too long, or else it will get dry and you won't have any drippings to add to the gravy.

### Gravy
1. Mix spices with a small amount of milk (enough to make a stirrable paste).  Don't add too much milk (remember you're adding cream after).
2. add butter and flour, begin to heat
3. cook for about 4 min "until raw flavor is gone"
4. add tomato puree or paste, add milk for consistency
5. put lid on and cook for 15 min on very low heat

### Combining them
1. when chicken is done, remove from skillet and pour all drippings / remnants into gravy pot
2. cut chicken into bite sized pieces
3. add chicken to gravy and let simmer for 2 min
4. add cream and mix

Serve with rice or naan
