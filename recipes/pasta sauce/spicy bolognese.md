# Spicy bolognese
Main article: http://www.geniuskitchen.com/recipe/real-italian-bolognese-sauce-83950

## Ingredients

- 1.5 kg plum tomatoes
- 1-2 tbsp olive oil
- 500g onion
- 6-8 cloves garlic
- 5-10 thai chilis
- 450g extra lean ground beef
- 300g ground pork

Seasonings:

- 2 tbsp dried oregano
- (marjoram?)
- 2 tbsp garlic powder
- pinch of powdered sage (not too much!)
- pinch of powdered thyme
- 1-2 tbsp worcestershire sauce
- salt
- pepper
- 2 bay leaves?

- butter for serving

## Directions

Tomatoes:

- boil very large pot of water
- cut a small X in the bottom of each tomato
- blanch by boiling for a minute then plunging in cold water and peeling off skin
- remove stems
- dice tomatoes into irregular shapes if necessary
- blend or mash into a puree

Aromatics:

- dice onions and fry to caramelize
- mince garlic and thai chilis
- reduce heat and add garlic, chilis and oregano, being careful not to burn dried oregano
- cook for 1-2 minutes

Combine:

- add aromatics to the tomatoes and begin to simmer
- fully cook and brown beef and pork
- add meat to simmering sauce
- add rest of the spices: garlic powder, powdered sage, powdered thyme, worcestershire, pepper and salt
- simmer for at least 30 min (or a few hours)



## Tips

- cooks down to 0.66 of its original mass
- try with: pancetta, or veal, or bacon
- try with: 2 carrots, 1 stalk celery, finely chopped
- try with: red wine OR white wine




# Pasta seasonings

509g onion
30g olive oil
20g butter
18g garlic
1592g tomatoes

chilis: 12g
sage: 1.50g
basil: 4.80g
garlic powder: 11g
thyme: 4.00g
oregano: 9.25g
worcester: 39g
salt: 12g
