# Pasta (or pizza) sauce
More info: https://www.buzzfeed.com/christinebyrne/how-to-make-tomato-sauce

This sauce works for either pizza or pasta and is delicious.
More info: http://slice.seriouseats.com/archives/2010/10/the-pizza-lab-homemade-pizza-sauce-new-york-style-recipe.html

## Ingredients

- 1 tbsp butter
- 1 tbsp olive oil
- 1 onion
- 3 cloves garlic
- dried oregano
- chili flakes
- san marzano tomatoes (28 oz)
- 1-2 tbsp worcestershire sauce


## Directions

- Chop and cook onions (see tip #1)
- Mince / shred garlic
- Turn heat down, add garlic, oregano and chili flakes to pan and cook for 1-2 minutes
- Mash / puree tomatoes and add to pan
- Season with worcestershire sauce, add salt to taste (note that canned tomatoes may already be salted)
- Simmer for at least 30 min


## Tips

- optionally: don't chop onions, just peel and cut in half and put them in after the tomatoes, removing once sauce is cooked




# Pasta seasonings

509g onion
30g olive oil
20g butter
32g garlic
1592g tomatoes

chilis: 10.7g
basil: 4g
thyme: 2g
oregano: 6g
worcester: 20g (27g)
salt: 10g
garlic powder: 6g
