- 2 russet potatoes
- 1 egg, beaten
- 1/2 cup shredded cheese
- 1/4 cup melted butter
- 1/4 cup green onion
- salt, pepper, pinch of cayenne, possibly paprika

Potato prep:
- Peel
- Shred using grater
- Soak in ice water
- Drain, squeeze out excess water and dry on paper towels (remove all the water)

Mix all ingredients and form into patties, bake at 400 for 35-40 minutes

# Tips

http://allrecipes.com/recipe/220520/classic-hash-browns/
