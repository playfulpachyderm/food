# Potatoes

- [Baked cheddar, eggs and potatoes](http://www.tasteofhome.com/recipes/baked-cheddar-eggs---potatoes) <-- add sriracha!
- [hash browns](/recipes/hash browns.md)

- [twice-baked potato with egg](https://www.youtube.com/watch?t=187&v=UgUBdWgeYI8)
- rosemary potatoes
- [scalloped (cheesy) potatoes](https://www.youtube.com/watch?v=F_X08IejW40)
- [loaded potatoes](http://www.delish.com/cooking/recipe-ideas/recipes/a46753/loaded-skillet-potato-skins-recipe/)
- [herb roasted potatoes](http://i.imgur.com/wv4rdV9.gifv)
- Helically cut potatoes on skewer: https://www.youtube.com/watch?v=FOv_YBDH6eg
- Hash brown quiche with bacon and cheese: https://www.youtube.com/watch?v=wi-FRxB6Yiw
- [pulled pork fries, quasi poutine](https://www.youtube.com/watch?v=7fTNtqwlWd8)
- [cheddar ranch potatoes](https://www.youtube.com/watch?v=YuCVpR51xJw) <-- boil potatoes with baking soda in the water, to create crunchier crust
- [potato bacon and cheddar casserole](https://www.youtube.com/watch?v=DMED5Ok4mTY)
- [potato sausage egg and cheese hash](http://www.tasteofhome.com/recipes/brunch-hash---egg-bake)
- [various different hashbrowns](https://www.buzzfeed.com/hannahmars/xx-hashbrown-recipes-cause-theyre-the-greatest-breakfast-car)
- [bacon and cheese potato casserole](https://www.youtube.com/watch?v=DMED5Ok4mTY)
- [mashed potatoes](https://www.youtube.com/watch?v=84i8Qdnyd0k)
- [hash brown cups](http://www.joyofbaking.com/breakfast/HashBrownBreakfastCups.html)
- [potato souffle](https://www.youtube.com/watch?v=HU9K8IqqAZk)
- [mashed potato variations](http://cdn.skim.gs/image/upload/v1456338938/msi/MashedPotatoes_tq4p1j.jpg)
- smashed roasted small potatoes

## Basic potato ingredients

- potato
- cheese
- egg
- butter
- pork meat (bacon, sausage, pulled pork, etc)
- fresh herbs

The above recipes consist mostly of re-arranging these ingredients in different combinations and patterns.
