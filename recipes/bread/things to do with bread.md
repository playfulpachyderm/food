# Things to do with bread

- bruschetta
- garlic bread
- [Bread stuffing](https://www.thekitchn.com/how-to-make-easy-thanksgiving-stuffing-cooking-lessons-from-the-kitchn-180112)
- [French onion soup](https://www.thekitchn.com/how-to-make-french-onion-soup-cooking-lessons-from-the-kitchn-110054)
- [more french onion soup](http://www.delish.com/cooking/recipe-ideas/recipes/a44297/easy-french-onion-soup-recipe/)
- [Panade (stale bread bake)](https://www.thekitchn.com/how-to-make-a-panade-114846)
- Things to do with stale bread: https://www.thekitchn.com/waste-not-5-ways-to-use-leftov-100588
- [bread casserole](http://www.eatingwell.com/recipe/250815/roasted-garlic-leek-bread-casserole/)
	- add chicken!

- Sandwiches (see section)

- Catalogue: https://en.wikipedia.org/wiki/List_of_bread_dishes
