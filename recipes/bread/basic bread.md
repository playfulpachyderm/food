# Bread
(original: https://www.marthastewart.com/921547/how-make-classic-white-bread?slide=3380736)

Time: 20+10 mins work time, 60+60+30 mins cook time (3 hrs total)
Makes: 2 loaves

## Equipment

- large mixing bowl
- 2 bread trays, 4.5" x 8.5"
- cup for melting butter
- parchment (if lazy)

## Ingredients

- 7g activated dry yeast
- 500g warm water (2 cups), should be at ~50 degrees C
- 30g sugar (2 tbsp)
- 10g salt (2 tsp)
- 50g butter (1/4 cup)
- 20g more butter (greasing pan and brushing loaf)
- 800g+ flour (6-7 cups)


## Instructions

- activate yeast in warm water (10 min)
- whisk in sugar, salt, melted butter
- add half the flour and kneed until mixed
- gradually add rest of the flour, bit by bit and mix in.  Add until result is somewhere between doughy and sticky (not too sticky)
- kneed to mix (5 min)
- cover and leave to rise in warm spot, should double in size (60 min)

- grease bread pans
- kneed the air out of dough
- split into 2, form into loaves and put in pans
- let rise again to double size in pans (45-60 min)

- brush loaves with butter
- bake at 450 for 30-40 min, reducing temperature to 400 halfway


## Tips

- possible substitutions:
	- olive oil instead of butter
	- lard instead of butter
	- honey instead of sugar
	- whole wheat flour
	- add an egg? (need recipe for this)
	- milk instead of water

- try the folding trick when putting bread in the pans (from original article)


## Nutrition

Food									Calories	Carbs		Protein		Fat			Sugar

800g flour                              2664        584         104         8           0
30g sugar                               116         30          0           0           30
60g butter                              428         0           0           51          0

Total                                   3208        614         104         59          30
                                                    (77%)       (13%)       (17%)       (5%)


1387
